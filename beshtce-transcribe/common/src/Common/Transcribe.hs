module Common.Transcribe
  ( latinToArmenian
  ) where

import qualified Data.Text as T

latinToArmenian :: T.Text -> Either () T.Text
latinToArmenian = right
