{ obelisk ? import ./.obelisk/impl {
    system = builtins.currentSystem;
    # iosSdkVersion = "10.2";
    config.android_sdk.accept_license = true;
  }
}:
let
  appId = "world.exya.beshtce.transcribe";
  appName = "Bestche Transcription Service";
in with obelisk;
project ./. ({ ... }: {
  android.applicationId = appId;
  android.displayName = appName;
  ios.bundleIdentifier = appId;
  ios.bundleName = appName;
  __withGhcide = true;
})
