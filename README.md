# Baeştçe

Baeştçe is a conlang created for an unpublished mid-future setting.
This repository contains all materials related to this language that one might
want to check into source control.

For example:
  - Code for languge utilities
  - Exported data (e.g. dictionaries)
  - Markdown and LaTeX
  - Web pages
